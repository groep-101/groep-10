﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QuizWebsite
{    public class Antwoord
    {
        [Key]
        public int antwoordID { get; set; }
        public string antwoord { get; set; }
        public int goed { get; set; }
        public int vraagID { get; set; }
    }
}
