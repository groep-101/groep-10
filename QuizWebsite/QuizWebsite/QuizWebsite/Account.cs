﻿using System.ComponentModel.DataAnnotations;

namespace QuizWebsite
{
    public class Account
    {
        [Required()]
        public int accountID { get; set; }
        [Required()]
        public int persoonID { get; set; }
        [Required(), MinLength(2), MaxLength(30)]
        public string gebruikersnaam { get; set; }
        [Required()]
        public string wachtwoord { get; set; }
        [Required()]
        public string email { get; set; }
        [Required()]
        public int rolID { get; set; }
        //SELECT `account`.`accountID`,
        //`account`.`persoonID`,
        //`account`.`gebruikersnaam`,
        //`account`.`email`,
        //`account`.`quizID`,
        //`account`.`rolID`
        //FROM `projectweb`.`account`;
    }
}
