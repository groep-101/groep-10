using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace QuizWebsite.Pages
{
    public class ResultatenModel : PageModel
    {
        public int accountID
        {
            get
            {
                string AccountIDStr = HttpContext.Session.GetString("accountID");
                if (AccountIDStr != null)
                {
                    int accountID = Convert.ToInt32(AccountIDStr);
                    return accountID;
                }
                else
                {
                    return -1;
                }
            }

        }
        public List<QuizResultaten> Quizresultaten = new List<QuizResultaten>();
        public IActionResult OnGet()
        {
            if (accountID == -1)
            {
                
                return Redirect("/Error");
            }
            Quizresultaten = QuizRepository.QuizCijfer(accountID);
            return Page();
        }

    }
}
