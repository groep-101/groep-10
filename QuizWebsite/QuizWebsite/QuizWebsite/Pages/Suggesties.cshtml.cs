using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace QuizWebsite.Pages
{
    public class SuggestiesModel : PageModel
    {
        public int accountID
        {
            get
            {
                string AccountIDStr = HttpContext.Session.GetString("accountID");
                if (AccountIDStr != null)
                {
                    int accountID = Convert.ToInt32(AccountIDStr);
                    return accountID;
                }
                else
                {
                    return -1;
                }
            }

        }
        public IActionResult OnGet()
        {
            if (accountID == -1)
            {
                return Redirect("/Error");
            }
            return Page();
        }

        [BindProperty]
        public Email sendmail { get; set; }
        public async Task OnPost()
        {
            string Body = sendmail.Body;
            MailMessage mm = new MailMessage();
            mm.Subject = "Suggestie";
            mm.Body = Body;
            mm.To.Add("wapplesaus@gmail.com");
            mm.IsBodyHtml = false;
            mm.From = new MailAddress("wapplesaus@gmail.com");
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.Port = 587;
            smtp.UseDefaultCredentials = false;
            smtp.EnableSsl = true;
            smtp.Credentials = new System.Net.NetworkCredential("wapplesaus@gmail.com", "Wapple2021");
            await smtp.SendMailAsync(mm);
            ViewData["Message"] = "Het bericht is verzonder naar" + sendmail.To + " Wapplesaus";
        }
    }
}
