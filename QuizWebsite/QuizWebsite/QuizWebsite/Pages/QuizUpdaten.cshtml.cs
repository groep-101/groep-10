using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace QuizWebsite.Pages
{
    public class QuizUpdatenModel : PageModel
    {
        public int accountID
        {
            get
            {
                string AccountIDStr = HttpContext.Session.GetString("accountID");
                if (AccountIDStr != null)
                {
                    int accountID = Convert.ToInt32(AccountIDStr);
                    return accountID;
                }
                else
                {
                    return -1;
                }
            }

        }
        public int Beheerder
        {
            get
            {
                string beheerderIDStr = HttpContext.Session.GetString("beheerder");
                if (beheerderIDStr != null)
                {
                    int beheerderID = Convert.ToInt32(beheerderIDStr);
                    return beheerderID;
                }
                else
                {
                    return -1;
                }
            }

        }
        public List<Quiz> quizes = new List<Quiz>();

        public IActionResult OnGet()
        {
            if (accountID == -1)
            {
                return Redirect("/Error");
            }
            quizes = QuizRepository.GetQuizAccount(accountID);
            if (Beheerder == -1)
            {
                return Redirect("/Error");
            }
            return Page();
        }
        public IActionResult OnPostKiesQuiz([FromForm] int quizID)
        {
            if (QuizRepository.QuizControlleren(quizID, accountID))
            {
                return RedirectToPagePermanent("/Vraagaanmaken", new { quizID = quizID });
            }
            return RedirectToPage("/homelogin");
        }
    }
}
