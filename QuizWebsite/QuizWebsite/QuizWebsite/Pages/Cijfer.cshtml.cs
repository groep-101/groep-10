using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace QuizWebsite.Pages
{
    public class CijferModel : PageModel
    {
        public List<Quiz> Quizlijst { get; set; }
        public decimal cijfer { get; set; }
        public int quizID { get; set; }
        public int accountID { get; set; }
        public void OnGet()
        {
            var cijferStr = Request.Query["cijfer"];
            decimal cijferInt;
            if (decimal.TryParse(cijferStr, NumberStyles.Any, CultureInfo.InvariantCulture, out cijferInt))
            {
                cijfer = cijferInt;
            }
            var quizIDStr = Request.Query["quizID"];
            if (Int32.TryParse(quizIDStr, out int quizIDInt))
            {
                Quizlijst = QuizRepository.GetQuizNaam(quizIDInt);
                quizID = quizIDInt;
            }
            var accountIDStr = Request.Query["accountID"];
            if (Int32.TryParse(accountIDStr, out int accountIDInt))
            {
                accountID = accountIDInt;
            }
            if (ModelState.IsValid)
            { 
            QuizRepository.CijferOpslaan(cijfer, quizID, accountID);
            }

        }
    }
}
