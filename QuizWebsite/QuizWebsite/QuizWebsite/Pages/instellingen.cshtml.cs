using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace QuizWebsite.Pages
{
    public class instellingenModel : PageModel
    {
        public int persoonID { get
            {
                    string strAccount = HttpContext.Session.GetString("persoonID");
                    if (strAccount != null)
                    {
                        int accountID = Convert.ToInt32(strAccount);
                        return accountID;
                    }
                    else
                    {
                        return -1;
                    }
            }
        }

        //[BindProperty] public Persoon Persoon { get; set; }

        public List<Klas> Klassen = new List<Klas>();

        public IActionResult OnGet() 
        {
            if (persoonID == -1)
            {
                return Redirect("/loginpage");
            }
            //this.Persoon = loginRepository.GetOnePersoon(persoonID);
            this.Klassen = loginRepository.Klassen();
            VeranderKlas.PersoonId = persoonID;
            VeranderKlas.Klas = loginRepository.GetKlasByPersoonId(persoonID);
            return Page();
        }
        public IActionResult OnPostVeranderen(VeranderKlas veranderKlas)
        {
            if (ModelState.IsValid)
            { 
                var modifyklas = loginRepository.ModifyKlas(veranderKlas.PersoonId, veranderKlas.Klas);
            } 
            return Page();
        }

        [BindProperty] public VeranderKlas VeranderKlas { get; set; } = new VeranderKlas();

        public IActionResult OnPostUpdate(int idklas)
        {
            var updateKlas = loginRepository.Update(idklas, persoonID);
            return Page();
        }
        public IActionResult OnPostUitloggen()
        {
            return Redirect("/inloggencode");
        }
    }
}
