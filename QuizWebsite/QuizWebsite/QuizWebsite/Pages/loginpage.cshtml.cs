using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace QuizWebsite.Pages
{
    public class loginpageModel : PageModel
    {
        public int accountID { get; set; }
        public bool Invalidlogin { get; private set; }

        public void OnGet()
        {
            //Hierdoor removed hij automatisch het id elke keer dat je inlogt. Zo kan je gewoon een button hebben 
            //die naar een andere pagina defineert
            HttpContext.Session.Remove("accountID");
            HttpContext.Session.Remove("PersoonID");
            HttpContext.Session.Remove("gebruiker");
            HttpContext.Session.Remove("beheerder");
        }

        public IActionResult OnPostChecklogin(string gebruikersnaam, string wachtwoord)
        {
            var checklogin = loginRepository.AccountCheck(gebruikersnaam, wachtwoord);
            if (checklogin == null)
            {
                Invalidlogin = true;
                return Page();
            }
            else
            {
                HttpContext.Session.SetString("accountID", checklogin.accountID.ToString());
                HttpContext.Session.SetString("persoonID", checklogin.persoonID.ToString());
                string accountID = HttpContext.Session.GetString("accountID");


                if (!string.IsNullOrEmpty(accountID))
                {
                    ViewData["ingelogd"] = "1";
                }
                if (Int32.TryParse(accountID, out int accountIDInt))
                {
                    var beheerder = loginRepository.BeheerderCheck(accountIDInt);
                    if (beheerder == true)
                    {
                        HttpContext.Session.SetString("beheerder", "1");
                    }
                    else 
                    {
                        HttpContext.Session.SetString("beheerder", "-1");
                        HttpContext.Session.SetString("gebruiker", "1");
                    }
                }
                return Redirect("/homelogin");
            }
        }
    }
}
