using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace QuizWebsite.Pages
{
    public class UitnodigenModel : PageModel
    {
        public int accountID
        {
            get
            {
                string AccountIDStr = HttpContext.Session.GetString("accountID");
                if (AccountIDStr != null)
                {
                    int accountID = Convert.ToInt32(AccountIDStr);
                    return accountID;
                }
                else
                {
                    return -1;
                }
            }

        }
        public int Beheerder
        {
            get
            {
                string beheerderIDStr = HttpContext.Session.GetString("beheerder");
                if (beheerderIDStr != null)
                {
                    int beheerderID = Convert.ToInt32(beheerderIDStr);
                    return beheerderID;
                }
                else
                {
                    return -1;
                }
            }

        }
        public List<Klas> klasid { get { return QuizRepository.GetKlas(); } }
        public List<Quiz> quizid { get { return QuizRepository.GetQuiz(); } }
        public IActionResult OnGet()
        {
            if (accountID == -1)
            {
                return Redirect("/Error");
            }
            if (Beheerder == -1)
            {
                return Redirect("/Error");
            }
            return Page();
        }

        [BindProperty]
        public Email sendmail { get; set; }

        public async Task OnPost()
        {
            string klassennaam = Request.Form["klas"];
            List<string> emails = QuizRepository.GetEmailByClass(Convert.ToInt32(klassennaam));
            string quiznaam = Request.Form["quiz"];
            string Body = sendmail.Body;
            MailMessage mm = new MailMessage();
            foreach (var item in emails)
            {
                mm.To.Add(item);
            }

            mm.Subject = quiznaam;
            mm.Body = Body;
            mm.IsBodyHtml = false;
            mm.From = new MailAddress("wapplesaus@gmail.com");
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.Port = 587;
            smtp.UseDefaultCredentials = false;
            smtp.EnableSsl = true;
            smtp.Credentials = new System.Net.NetworkCredential("wapplesaus@gmail.com", "Wapple2021");
            await smtp.SendMailAsync(mm);
            ViewData["Message"] = "Het bericht is verzonder naar" + sendmail.To + quiznaam + klassennaam;
        }
    }
}