using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace QuizWebsite.Pages
{
    public class VraagaanmakenModel : PageModel
    {

        //public List<Quiz> quizid { get { return QuizRepository.GetQuiz(); } }
        public List<QuizVragen> vragenlijst { get; set; }
        public List<QRYQuizVragen> vragenmetantwoord { get; set; }
        public int quizIDInt;
        public IActionResult OnGet([FromQuery] string quizID)
        {
            
            if ((quizIDInt = GetCurrentQuiz(quizID)) != -1)
            {
                if (quizID != null && quizID != "0")
                {
                    vragenlijst = QuizRepository.QuizVragen(quizIDInt);
                    vragenmetantwoord = QuizRepository.GetVraagMetAntwoorden(quizIDInt);
                    return Page();
                }
                else
                {
                    return Redirect("/Error");
                }
                
            }
            else 
            {
                vragenlijst = QuizRepository.QuizVragen(quizIDInt);
                vragenmetantwoord = QuizRepository.GetVraagMetAntwoorden(quizIDInt);
                return Page();
            }

        }
        public int GetCurrentQuiz(string quizIDStr)
        {
            if (Int32.TryParse(quizIDStr, out int quizid))
            {
                return quizid;
            }
            return -1;
        }
        public IActionResult OnPostVraagAanmaken(string vraag, int quizIDInt)
        {
            if (ModelState.IsValid)
            {
                QuizRepository.VraagAanmaken(vraag, quizIDInt);
                vragenmetantwoord = QuizRepository.GetVraagMetAntwoorden(quizIDInt);
                return RedirectToPage("/VraagAanmaken", new { quizID = quizIDInt });
            }
            else 
            {
                return Redirect("/Error");
            }
        }
        public IActionResult OnPostVraagVerwijderen(int vraagID, int quizIDInt)
        {
            if (ModelState.IsValid)
            {
                QuizRepository.VraagVerwijderen(vraagID);
                vragenmetantwoord = QuizRepository.GetVraagMetAntwoorden(quizIDInt);
                return RedirectToPage("/VraagAanmaken", new { quizID = quizIDInt });
            }
            return RedirectToPage("/VraagAanmaken", new { quizID = quizIDInt });
        }
        public IActionResult OnPostAntwoordToevoegen(string antwoord, int huidigevraag, int quizIDInt)
        {
            if (ModelState.IsValid)
            {
                QuizRepository.AntwoordToevoegen(antwoord, huidigevraag);
                vragenmetantwoord = QuizRepository.GetVraagMetAntwoorden(quizIDInt);
                return RedirectToPage("/VraagAanmaken", new { quizID = quizIDInt });
            }
            return RedirectToPage("/VraagAanmaken", new { quizID = quizIDInt });
        }
        public IActionResult OnPostUpdateAntwoord(int antwoordID, int ischecked, int quizIDInt)
        {
            if (ModelState.IsValid)
            {
                bool is_checked = (ischecked == antwoordID);
               //bool is_checked = (ischecked == "on");
                QuizRepository.UpdateAntwoord(antwoordID, is_checked);
                vragenmetantwoord = QuizRepository.GetVraagMetAntwoorden(quizIDInt);
                return RedirectToPage("/VraagAanmaken", new { quizID = quizIDInt });
            }
            return RedirectToPage("/VraagAanmaken", new { quizID = quizIDInt });
        }
        public IActionResult OnPostAntwoordVerwijderen(int antwoordID, int quizIDInt)
        {
            if (ModelState.IsValid)
            {
                QuizRepository.AntwoordVerwijderen(antwoordID);
                vragenmetantwoord = QuizRepository.GetVraagMetAntwoorden(quizIDInt);
                return RedirectToPage("/VraagAanmaken", new { quizID = quizIDInt });
            }
            return RedirectToPage("/VraagAanmaken", new { quizID = quizIDInt });
        }

    }
}
