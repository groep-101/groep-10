using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace QuizWebsite.Pages
{
    public class QuizJoinenModel : PageModel
    {
        public int accountID
        {
            get
            {
                string AccountIDStr = HttpContext.Session.GetString("accountID");
                if (AccountIDStr != null)
                {
                    int accountID = Convert.ToInt32(AccountIDStr);
                    return accountID;
                }
                else
                {
                    return -1;
                }
            }

        }
        public IActionResult OnGet([FromForm] int quizID)
        {
            if (accountID == -1)
            {
                return Redirect("/Error");
            }
            return Page();
        }
        public IActionResult OnPostJoinQuiz([FromForm] int quizID)
        {
            if (QuizRepository.QuizJoinControle(quizID))
            {
                return RedirectToPage("/VraagBeantwoorden", new { quizID = quizID, accountID = accountID });
            }
            return RedirectToPage("/homelogin");
        }
    }
}
