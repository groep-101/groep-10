using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace QuizWebsite.Pages
{
    public class QuizmakenModel : PageModel
    {
        public int accountID
        {
            get
            {
                string AccountIDStr = HttpContext.Session.GetString("accountID");
                if (AccountIDStr != null)
                {
                    int accountID = Convert.ToInt32(AccountIDStr);
                    return accountID;
                }
                else
                {
                    return -1;
                }
            }

        }
        public int Beheerder
        {
            get
            {
                string beheerderIDStr = HttpContext.Session.GetString("beheerder");
                if (beheerderIDStr != null)
                {
                    int beheerderID = Convert.ToInt32(beheerderIDStr);
                    return beheerderID;
                }
                else
                {
                    return -1;
                }
            }

        }
        public IActionResult OnGet()
        {
            if (accountID == -1)
            {
                return Redirect("/Error");
            }
            if (Beheerder == -1)
            {
                return Redirect("/Error");
            }
            return Page();
        }

        public IActionResult OnPostQuizAanmaken([FromForm]string naam)
        {
            if (ModelState.IsValid)
            {
                var idquiz = QuizRepository.QuizAanmaken(naam,accountID);
                //return this.RedirectToAction
                   // ("quizID", new { quizID = "queryStringValue1" });
                //return RedirectToPage($"/Vraagaanmaken", new { idquiz = idquiz });
                return RedirectToPage ("/VraagAanmaken", new { quizID = idquiz, accountID = accountID });
            }
            else
            {
                return Page();
            }
        }
    }
}
