using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace QuizWebsite.Pages
{
    public class VraagBeantwoordenModel : PageModel
    {
        public List<QuizVragen> vragenlijst { get; set; }
        public List<QRYQuizVragen> vragenmetantwoord { get; set; }

        public int quizID;
        public int accountID
        {
            get
            {
                string AccountIDStr = HttpContext.Session.GetString("accountID");
                if (AccountIDStr != null)
                {
                    int accountID = Convert.ToInt32(AccountIDStr);
                    return accountID;
                }
                else
                {
                    return -1;
                }
            }

        }

        public IActionResult OnGet()
        {
            if (accountID == -1)
            {
                return Redirect("/Error");
            }
            
            var quizIDStr = Request.Query["quizID"];

            if ((this.quizID = GetCurrentQuiz(quizIDStr)) != -1)
            {
                if (quizIDStr != "" && quizIDStr != "0")
                {
                    vragenlijst = QuizRepository.QuizVragen(this.quizID);
                    vragenmetantwoord = QuizRepository.GetVraagMetAntwoorden(this.quizID);
                    return base.Page();
                }
                else
                {
                    return Redirect("/Error");
                }

            }
            else
            {
                return Redirect("/Error");
            }

        }
        public int GetCurrentQuiz(string quizIDStr)
        {
            if (Int32.TryParse(quizIDStr, out int quizid))
            {
                return quizid;
            }
            return -1;
        }

        public IActionResult OnPostQuizInleveren(int accountID, [FromForm] IEnumerable<string> vraagID, [FromForm] IEnumerable<string> antwoordID, int quizID)
        {
            int i = 0;
            int vraagInt = 0;
            int goed = 0;
            foreach (var antwoord in antwoordID)
            {
                QuizRepository.VraagBeantwoorden(accountID, Convert.ToInt32(vraagID.ElementAt(i)), Convert.ToInt32(antwoord));
                if (QuizRepository.VraagControlleren(accountID, Convert.ToInt32(vraagID.ElementAt(i)), Convert.ToInt32(antwoord)))
                {
                    goed++;
                }
                i++;
            }
            foreach (var vraag in vraagID)
            {
                vraagInt++;

            }
            //double cijfer = ((goed / vraagInt) * 100) * 0.1;
            double cijfer = (double)goed / vraagInt *100 *0.1;
            cijfer = Math.Round(cijfer, 1, MidpointRounding.ToEven);
            return RedirectToPage("/Cijfer", new { cijfer = cijfer, quizID= quizID, accountID = accountID });
        }

    }
}
