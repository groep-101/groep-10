using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace QuizWebsite.Pages
{
    public class signuppageModel : PageModel
    {
        public bool Invalidlogin { get; private set; }
        public IEnumerable<Account> accounts
        {
            get { return loginRepository.GetAlleAccounts(); }
        }

        public List<Account> account { get { return loginRepository.GetAlleAccounts(); } }

        public IActionResult OnPostSubmit([FromForm] string voornaam, string achternaam, string gebruikersnaam, string wachtwoord, string email, int klas)
        {
            if (ModelState.IsValid)
            {
                var addedAccount = loginRepository.AddPersoon(voornaam, achternaam, gebruikersnaam, wachtwoord, email, klas);
                return Redirect("/loginpage");
            }
            else
            {
                Invalidlogin = true;
                return Page();
            }
        }
    }
}
