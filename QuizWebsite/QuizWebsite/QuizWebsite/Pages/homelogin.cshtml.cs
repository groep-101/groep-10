using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace QuizWebsite.Pages
{
    public class homeloginModel : PageModel
    {
        public int accountID
        {
            get
            {
                string strAccount = HttpContext.Session.GetString("accountID");
                if (strAccount != null)
                {
                    int accountID = Convert.ToInt32(strAccount);
                    return accountID;
                }
                else
                {
                    return -1;
                }
            }
        }

        public Account account  { get { return loginRepository.GetAccount(accountID); } }

        public IActionResult OnGet()
        {
            if (accountID == -1)
            {
                return Redirect("/inloggen");
            }
            else
            {
                return Page();
            }
        }
        public IActionResult OnPostUitloggen()
        {
            return Redirect("/inloggen");
        }
    }   
}

