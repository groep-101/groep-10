﻿using System.ComponentModel.DataAnnotations;

namespace QuizWebsite
{
    public class Persoon
    {
        [Required()]
        public int persoonID { get; set; }
        [Required(), MinLength(2), MaxLength(30)]
        public string voornaam { get; set; }
        [Required()]
        public string achternaam { get; set; }
        public int klas { get; set; }
        //SELECT `persoon`.`persoonID`,
        //`persoon`.`voornaam`,
        //`persoon`.`achternaam`,
        //`persoon`.`klas`
        //FROM `quizwebsite`.`persoon`;
    }
}

