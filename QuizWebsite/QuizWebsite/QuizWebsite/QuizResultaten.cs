﻿namespace QuizWebsite
{
    public class QuizResultaten
    {
        public int cijferID { get; set; }
        public int cijfer { get; set; }
        public int quizID { get; set; }
        public int accountID { get; set; }
        public string naam { get; set; }
        public Quiz quiz { get; set; }
    }
}