﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using MySql.Data.MySqlClient;


namespace QuizWebsite
{

    public class QuizRepository
    {
        private static IDbConnection Connect()
        {
            return new MySqlConnection(
                "Server=127.0.0.1;Port=3306;" +
                "Database=quizwebsite;" +
                "Uid=root;Pwd=Test@1234;"
            );
        }
        public static List<Klas> GetKlas()
        {
            using var connection = Connect();
            var klasid = connection.Query<Klas>("SELECT * FROM klas");
            return klasid.ToList();
        }
        public static List<Quiz> GetQuiz()
        {
            using var connection = Connect();
            var quizid = connection.Query<Quiz>("SELECT * FROM quiz");
            return quizid.ToList();
        }

        public static List<Quiz> GetQuizAccount(int accountID)
        {
            using var connection = Connect();
            var quizid = connection.Query<Quiz>("SELECT * FROM quiz WHERE accountID = @accountID"
                , new {accountID = accountID });
            return quizid.ToList();
        }
        public static List<Quiz> GetQuizNaam(int quizID)
        {
            using var connection = Connect();
            var quizid = connection.Query<Quiz>("SELECT * FROM quiz WHERE quizID = @quizID"
             ,new {quizID = quizID });
            return quizid.ToList();
        }

        public static List<QuizVragen> QuizVragen(int quizid)
        {
            using var connection = Connect();
            var quizvragen = connection.Query<QuizVragen>("SELECT * FROM vraag WHERE quizID = @quizID"
            ,new {quizID = quizid });
            return quizvragen.ToList();
        }

        public static int QuizAanmaken(string naam, int accountID)
        {
            using var connection = Connect();
            int quizid = -1;
            var naamcontroleren = connection.ExecuteScalar<Boolean>(
                @"SELECT COUNT(1) = 1 FROM quiz WHERE naam = @naam"
                ,new {naam = naam});    
            if(!naamcontroleren)
            { 
                int RowsAffected = connection.Execute(
                @"INSERT INTO quiz (naam,accountID) VALUES (@naam,@accountID)"
                ,new {naam = naam, accountID = accountID});
                if(RowsAffected == 1)
                { 
                    quizid = connection.ExecuteScalar<int>(@"SELECT LAST_INSERT_ID()");
                }
            }
            return quizid;
        }
        public static bool QuizControlleren(int quizID, int accountID)
        {
            using var connection = Connect();
            var naamcontroleren = connection.ExecuteScalar<Boolean>(
                @"SELECT COUNT(1) = 1 FROM quiz WHERE quizID = @quizID AND accountID = accountID"
                , new { quizID = quizID, accountID = accountID });
            return naamcontroleren;
        }
        public static bool QuizJoinControle(int quizID)
        {
            using var connection = Connect();
            var naamcontroleren = connection.ExecuteScalar<Boolean>(
                @"SELECT COUNT(1) = 1 FROM quiz WHERE quizID = @quizID"
                , new { quizID = quizID});
            return naamcontroleren;
        }
        public static List<QuizResultaten> QuizCijfer(int accountID)
        {
            string sql = @"SELECT c.cijferID cijferID, c.cijfer cijfer,c.accountID accountID, q.naam naam, q.quizID  quizID   
                            FROM cijfer c 
								 LEFT JOIN quiz q on q.quizID = c.quizID
                            WHERE c.accountID= @accountID ORDER BY c.quizID ASC";
            using var connection = Connect();
            var quizvragens = connection.Query<QuizResultaten>(sql, param: new { accountID= accountID }).ToList();

            return quizvragens.ToList();
        }
        public static void VraagAanmaken(string vraag, int quizID)
        {
            using var connection = Connect();
                int RowsAffected = connection.Execute(
                @"INSERT INTO vraag (vraag,quizID) VALUES (@vraag, @quizID)"
                , new { vraag = vraag, quizID = quizID });
        }
        public static void VraagVerwijderen(int vraagID)
        {
            using var connection = Connect();
            int RowsAffected = connection.Execute(
            @"DELETE FROM antwoord WHERE (vraagID = @vraagID)"
            , new { vraagID = vraagID });
            if (RowsAffected >= 0)
            {
                int VraagRowsAffected = connection.Execute(
                @"DELETE FROM vraag WHERE (vraagID = @vraagID)"
                , new { vraagID = vraagID });
            }
        }
        public static void AntwoordToevoegen(string antwoord, int huidigevraag)
        {
            using var connection = Connect();
            int RowsAffected = connection.Execute(
            @"INSERT INTO antwoord (antwoord,goed,vraagID) VALUES (@antwoord,@goed, @vraagID)"
            , new { antwoord = antwoord,goed = 0, vraagID = huidigevraag });
        }
        public static void AntwoordVerwijderen(int antwoordID)
        {
            using var connection = Connect();
            int RowsAffected = connection.Execute(
            @"DELETE FROM antwoord WHERE (antwoordID = @antwoordID)"
            , new { antwoordID = antwoordID });
        }
        public static void UpdateAntwoord(int antwoordID, bool is_checked)
        {
            using var connection = Connect();
            int numRowsEffected = connection.Execute("UPDATE antwoord SET goed = @is_checked WHERE antwoordID = @antwoordID",
                new { antwoordID = antwoordID, is_checked = is_checked });
            
        }

        public static List<QRYQuizVragen> GetVraagMetAntwoorden(int quiz)
        {
            // Order by zorgt ervoor dat de antwoorden bij de vragen blijven.
            string sql = @"SELECT v.vraagID vraagID, v.vraag vraag, v.quizID  quizID, a.antwoordID antwoordID, a.antwoord antwoord, a.goed goed     
                            FROM quiz q 
								 LEFT JOIN vraag v on v.quizID = q.quizID 
                                 LEFT JOIN antwoord a on a.vraagID = v.vraagID 
                            WHERE q.quizID= @quizID ORDER BY v.vraagID ASC";

            using var connection = Connect();
            var quizvragens = connection.Query<QRYQuizVragen>(sql,param: new {quizID = quiz}).ToList();

            return quizvragens;

        }

        public static void VraagBeantwoorden(int accountID,int vraagID, int antwoordID)
        {
            using var connection = Connect();
            bool vraagaffected = connection.ExecuteScalar<bool>(
                @"SELECT COUNT(1) = 1 FROM beantwoord WHERE vraagID= @vraagID AND accountID = @accountID"
                , new { accountID = accountID, vraagID = vraagID, antwoordID = antwoordID });
            if (vraagaffected == true)
            {   
                int RowsAffected = connection.Execute(
                @"UPDATE beantwoord SET antwoordID = @antwoordID WHERE accountID = @accountID AND vraagID = @vraagID"
                , new { accountID = accountID, vraagID = vraagID, antwoordID = antwoordID });
            }
            else 
            {
                int RowsAffected = connection.Execute(
                @"INSERT INTO beantwoord (accountID,vraagID,antwoordID) VALUES (@accountID,@vraagID,@antwoordID)"
                , new { accountID = accountID, vraagID = vraagID, antwoordID = antwoordID });
            }
        }
        public static bool VraagControlleren(int accountID, int vraagID, int antwoordID)
        {
            string sql = @"SELECT CASE WHEN a.antwoordID = b.antwoordID
                     THEN '1' 
                        ELSE '0' 
                            END 
                                AS MyDesiredResult   
                            FROM antwoord a
								 LEFT JOIN beantwoord b on b.vraagID = a.vraagID
                            WHERE a.goed = true AND b.accountID = @accountID AND b.vraagID = @vraagID";

            using var connection = Connect();
            //var quizvragens = connection.Query<QuizBeantwoord>(sql, param: new { accountID=accountID ,vraagID = vraagID }).ToList()
            string quizvragens = connection.ExecuteScalar<string>(sql, param: new { accountID = accountID, vraagID = vraagID });
            bool goed = false;
            if (quizvragens == "1")
            {
                goed = true;
            }
            return goed;
        }
        public static void CijferOpslaan(decimal cijfer, int quizID, int accountID)
        {
            using var connection = Connect();
            bool vraagaffected = connection.ExecuteScalar<bool>(
                @"SELECT COUNT(1) = 1 FROM cijfer WHERE quizID= @quizID AND accountID = @accountID"
                , new { accountID = accountID, quizID = quizID, });
            if (vraagaffected == true)
            {
                int RowsAffected = connection.Execute(
                @"UPDATE cijfer SET cijfer = @cijfer WHERE accountID = @accountID AND quizID = @quizID"
                , new { accountID = accountID, quizID = quizID, cijfer= cijfer });
            }
            else 
            {
                int RowsAffected = connection.Execute(
                @"INSERT INTO cijfer (accountID,quizID,cijfer) VALUES (@accountID,@quizID,@cijfer)"
                , new { accountID = accountID, quizID = quizID, cijfer = cijfer});
            }
        }
        //MARTIJN ZIJN REPOSITORY

        public static List<string> GetEmailByClass(int klasid)
        {
            using var connection = Connect();
            var naam = connection.Query<string>("SELECT persoonid FROM persoon WHERE klas = @klasid",
                new { klasid = @klasid });
            List<string> emails = new List<string>();
            foreach (var persoonid in naam)
            {
                var email = connection.Query<string>("SELECT email FROM account WHERE persoonID = @persoonid",
                new { persoonid = @persoonid });
                emails.Add(email.First());
            }
            return emails;
        }
    }

}
