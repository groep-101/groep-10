﻿namespace QuizWebsite
{
    internal class QuizBeantwoord
    {
        public int beantwoordID { get; set; }
        public int accountID { get; set; }
        public int vraagID { get; set; }
        public int antwoordID { get; set; }
        public Antwoord Antwoord { get; set; }
    }
}