﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using MySql.Data.MySqlClient;

namespace QuizWebsite
{
    public static class loginRepository
    {
        private static IDbConnection Connect()
        {
            return new MySqlConnection(
                "Server=127.0.0.1;Port=3306;" +
                "Database=quizwebsite;" +
                "Uid=root;Pwd=Test@1234;"
            );
        }

        public static List<Account> GetAlleAccounts()
        {
            using var connect = Connect();
            var account = connect.Query<Account>("SELECT * FROM account");
            return account.ToList();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static Account GetAccount(int accountID)
        {
            using var connect = Connect();
            var account = connect.QuerySingleOrDefault<Account>("SELECT * FROM account WHERE accountID = @accountID",
                new { accountID = accountID });
            return account;
        }
        public static bool BeheerderCheck(int accountID)
        {
            using var connect = Connect();
            int rolID = connect.QuerySingleOrDefault<int>("SELECT rolID FROM account WHERE accountID = @accountID",
                new { accountID = accountID });
            bool beheerder = connect.ExecuteScalar<Boolean>(@"SELECT beheerder FROM rol WHERE rolID = @rolID"
            , new { rolID = rolID});
            return beheerder;
        }

        public static bool AddAccount(Account account)
        {
            using var connection = Connect();
            int numRowEffected = connection.Execute(
                @"INSERT INTO account (gebruikersnaam, wachtwoord, email, rolID) 
                VALUES(@gebruikersnaam, @wachtwoord, @email, @rolID)",
                account);
            return numRowEffected == 1;
        }

        public static Account AccountCheck(string gebruikersnaam, string wachtwoord)
        {
            using var connect = Connect();
            var check = connect.QuerySingleOrDefault<Account>("SELECT * FROM account WHERE gebruikersnaam = @gebruikersnaam AND wachtwoord = @wachtwoord",
                new { gebruikersnaam = gebruikersnaam, wachtwoord = wachtwoord });
            return check;
        }

        public static bool Checking(Account account)
        {
            using var connect = Connect();
            bool bestaat = connect.ExecuteScalar<Boolean>(
                @"SELECT COUNT(1) = 1 FROM account WHERE accountID = @accountID"
            , new { accountID = account.accountID });
            return bestaat;
        }
        //public static Account CheckInlogGasten(string uniekecode)
        //{
        //    using var connect = Connect();
        //    var check = connect.QuerySingleOrDefault<Account>("SELECT * FROM bruidspaar WHERE uniekecode = @uniekecode",
        //        new { uniekecode = uniekecode });
        //    return check;
        //}

        ///////////////////////////////////personen////////////////////////////////////////////////////////

        public static List<Persoon> GetAllePersonen(int persoonID)
        {
            using var connect = Connect();
            var persoon = connect.Query<Persoon>("SELECT * FROM persoon");
            return persoon.ToList();
        }
        public static bool AddPersoon(string voornaam, string achternaam, string gebruikersnaam, string wachtwoord, string email, int klas)
        {
            using var connect = Connect();
            var check = connect.ExecuteScalar<Boolean>(@"SELECT COUNT(1) = 1 FROM persoon WHERE voornaam = @voornaam AND achternaam = @achternaam"
                , new { voornaam = voornaam, achternaam = achternaam });
            check = connect.ExecuteScalar<Boolean>(@"SELECT COUNT(1) = 1 FROM account WHERE gebruikersnaam = @gebruikersnaam AND wachtwoord = @wachtwoord AND email = @email"
                , new { gebruikersnaam = gebruikersnaam, wachtwoord = wachtwoord, email = email});
            if (!check)
            {
                var addpersoon = connect.Execute(
                    @"INSERT INTO persoon (voornaam, achternaam, klas) VALUES (@voornaam, @achternaam, @klas)"
                , new { voornaam = voornaam, achternaam = achternaam, klas = klas});
            }
            var persoonid = connect.ExecuteScalar<int>(@"SELECT persoonID FROM quizwebsite.persoon WHERE voornaam= @voornaam AND achternaam = @achternaam AND klas = @klas" 
                , new { voornaam = voornaam, achternaam = achternaam, klas = klas });
            if (!check)
            {
                var addaccount = connect.Execute(
                    @"INSERT INTO account (persoonid, gebruikersnaam, wachtwoord, email, rolid) VALUES (@persoonid, @gebruikersnaam, @wachtwoord, @email, @rolid)"
                , new { persoonid = persoonid, gebruikersnaam = gebruikersnaam, wachtwoord = wachtwoord, email = email, rolid = "1" });
            }
            return check;
        }
        public static bool Update(int klasId, int persoonId)
        {
            using var connection = Connect();
            int numRowsEffected = connection.Execute("UPDATE persoon WHERE klas = @klas AND persoonid = @persoonid",
                new {klas = klasId, persoonid = persoonId });
            return numRowsEffected == 1;
        }
        public static Persoon GetOnePersoon(int persoonId)
        {
            using var connection = Connect();
            Persoon persoon = connection.QuerySingle<Persoon>("Select P.* FROM persoon as P JOIN account as A ON A.persoonID = P.persoonID WHERE A.accountID = @loginUserId "
                , new { loginUserId = persoonId });
            return persoon; 
        }
        public static List<Klas> Klassen()
        {
            using var connection = Connect();
            List<Klas> klassen = connection.Query<Klas>("Select * FROM klas ORDER BY klasid ASC").ToList();
            return klassen;
        }

        public static bool ModifyKlas(int persoonid, int klas)
        {
            using var connection = Connect();
            int numRowEffected = connection.Execute(
                @"UPDATE persoon SET klas = @klas WHERE persoonid = @persoonid",
                new { klas = klas, persoonid = persoonid });
            return numRowEffected == 1;
        }
        public static int GetKlasByPersoonId(int persoonID)
        {
            using var connect = Connect();
            var klas = connect.QueryFirst<int>("SELECT klas FROM persoon WHERE persoonID = @persoonID", new { persoonID });
            return klas;
        }

    }
}
