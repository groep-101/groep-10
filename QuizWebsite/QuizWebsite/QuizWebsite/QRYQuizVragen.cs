﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizWebsite
{
    public class QRYQuizVragen
    {
        public int quizID;
        public string naam;
        public int vraagID;
        public string vraag;
        public int antwoordID;
        public string antwoord;
        public bool goed;
    }
}
