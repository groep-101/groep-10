﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizWebsite
{
    public class VeranderKlas
    {
        public int Klas { get; set; }
        public int PersoonId { get; set; }
    }
}
