﻿using System.ComponentModel.DataAnnotations;

namespace QuizWebsite
{
    public class Klas
    {
        [Required()]
        public int klasid { get; set; }
        public int klas { get; set; }
        //SELECT `klas`.`klasid`
        //FROM `quizwebsite`.`klas`;
    }
}
