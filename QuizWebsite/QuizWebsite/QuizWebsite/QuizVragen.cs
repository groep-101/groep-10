﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QuizWebsite
{
    public class QuizVragen
    {
        public int vraagID { get; set; }
        public string vraag { get; set; }
        public int quizID { get; set; }
        public Antwoord Antwoord { get; set; }
    }
}
